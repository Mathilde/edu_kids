/* Imen, Mathilde et Mohamed - Edu'Kids */

Il faut télécharger 2 librairies dans processing :
- Game Control Plus (pour la manette)
- Websockets (pour la reconnaissance vocale)
- Sound (pour l'animation de la bonne réponse)

Ensuite, lancer le projet.
Pour utiliser la reconnaissance vocale, il faut lancer le site suivant :  https://codepen.io/getflourish/pen/NpBGqe
et modifier la ligne : "recognition.lang = "en-US";" par la ligne "recognition.lang = "fr-FR";"

Le thème par défaut est le thème "animaux".
Pour le modifier (vous devez utiliser une manette), il faut maintenir le bouton "B" appuyé et se déplacer avec le joystick pour sélectionner le thème voulu.

Vous pouvez maintenant commencer la partie en cliquant sur le bouton "Let's Play" avec la manette (bouton "A") ou la souris.

Pour sélectionner la réponse voulue, vous pouvez utiliser la reconnaissance vocale ou bien la manette (toujours avec le bouton "A").

Le score est consultable depuis la page d'accueil en cliquant sur le bouton "Score" avec la manette (bouton "A") ou la souris.


Information : pour utiliser la reconnaissance vocale, il faut connecter des écouteurs et parler distinctement (en français) dans le micro.

Bonne partie ;)