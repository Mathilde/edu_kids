// Classe représentant la page d'accueil
class Accueil {
  
  // Positionnement cube Score
  int xminScore = width/2-300;
  int yminScore = height/2+100;
  int xmaxScore = xminScore + 200;
  int ymaxScore = yminScore + 200;  
  
  // Positionnement cube Exit
  int xminExit = width/2+100;
  int yminExit = height/2+100;
  int xmaxExit = xminExit + 200;
  int ymaxExit = yminExit + 200;
  
  // Positionnement cube Start
  int xminStart= width/2-100;
  int yminStart = height/2-100;
  int xmaxStart = xminStart + 200;
  int ymaxStart = yminStart + 200; 
  
  Partie partie = null;
  Score score = null;
  
  // Thème par défaut
  String theme = "animaux";
  
  // Variables pour savoir quelle page afficher
  private boolean endGame;
  private boolean displayScore;
  private boolean partieStart;
  
  PImage background;  
  float angle1;
  
  Accueil() {
    endGame = false;
    displayScore = false;    
    partieStart = false;
    partie = new Partie(theme);
    score = new Score();
  }
  
  void draw() { 
    if (!partieStart && !displayScore) { 
      
      // affichage du background
      background = loadImage("images/background.jpg"); 
      textureMode(NORMAL);
      fill(55);
      stroke(color(44,48,32));
      smooth();
      background(background);
      PFont F = createFont("Georgia",64);
      
      
      // S'il n'y a pas de manette connectée, on affiche un message pour avertir l'utilisateur
      if(stick == null) {
        textSize(40);
        textAlign(CENTER);
        fill(255, 0, 0);
        text("Veuillez relancer le jeu avec une manette pour pouvoir jouer avec une manette, \n sinon vous pouvez utiliser la reconnaissance vocale !", width/2, height/3);
      }
      
      // Affichage du tritre de l'accueil
      pushMatrix();
      translate(470, 50);
      textFont(F);
      textSize(68);
      fill(253 ,238 ,0);
      text(TEXT_GAME_NAME, width/4, 100);
      textAlign(LEFT);
      popMatrix();
   
      // Récupération des actions de la manette
      if(stick != null) {
        getUserInput();
        displayJoystick();
      }
    
      lights();
      
      // Affichage du cube score
      drawCube(xminScore+100, yminScore+100 , -130, 190, angle1, "Score");
      
      // Affichage du cube exit
      drawCube(xminExit+100, yminExit+100, -130, 190, angle1, "Exit");
       
      // Affichage du cube start  
      drawCube(xminStart+100, yminStart+100, -130, 190, angle1, "Start");
      
      // Affichage du footer
      displayFooter();     
    }
    
    // Affichage de la partie
    if(partieStart) {   
      partie.draw();
      if (partie.getEndPartie()){
        partieStart = false; 
        // Enregistrement du score de la dernière partie
        score.setScoreLastGame(partie.getScore());
      }
    }
  
    // Affichage de l'écran score
    if(displayScore) {
      displayScore = score.getDisplay();
      score.setDisplay(true);
      score.draw();
    }
    
    if(endGame) {    
      exit(); 
    }
  }
  
  // Méthode pour récupérer l'action avec la manette de l'utilisateur 
  public void getUserInput() {
      boolean clickA;
      boolean clickB;
      px = map(stick.getSlider(1).getValue(), -1, 1, 0, width);
      py = map(stick.getSlider(0).getValue(), -1, 1, 0, height);
      clickA = stick.getButton(0).pressed();
      clickB = stick.getButton(1).pressed();
      
      // Si on clique sur le cube Start
      if(clickA && px > xminStart && px < xmaxStart && py > yminStart && py < ymaxStart) {
        partie = new Partie(theme);
        partieStart = true; 
      }
      // Si on clique sur le cube Exit
      if(clickA && px > xminExit && px < xmaxExit && py > yminExit && py < ymaxExit) {      
        endGame = true;
      }
      // Si on clique sur le cube Score
      if(clickA && px > xminScore && px < xmaxScore && py > yminScore && py < ymaxScore) {
        score.setDisplay(true);
        displayScore = true;
      }
      clickA = false;
      
      handleMenuTheme(clickB);
  }
  
  // Affichage du menu pour le choix du thème
  public void handleMenuTheme(boolean clickB){
    int sizeImageMenu = 150;
    if (clickB && !clickBPressed){
        xMenu = px;
        yMenu = py;
        clickBPressed = true;
      }
      
      int select = 0;
      
      if (clickBPressed){
        if(px > xMenu + sizeImageMenu && px < xMenu + sizeImageMenu*2 && py < yMenu + sizeImageMenu/2  && py > yMenu - sizeImageMenu/2 ) {      
          select = 1;
        }
        if(px > xMenu - sizeImageMenu*2 && px < xMenu - sizeImageMenu && py < yMenu + sizeImageMenu/2  && py > yMenu - sizeImageMenu/2 ) {      
          select = 2;
        }
        if(px > xMenu - sizeImageMenu/2 && px < xMenu + sizeImageMenu/2 && py < yMenu - sizeImageMenu  && py > yMenu - sizeImageMenu*2 ) {      
          select = 3;
        }
        if(px > xMenu - sizeImageMenu/2 && px < xMenu + sizeImageMenu/2 && py < yMenu + sizeImageMenu*2  && py > yMenu + sizeImageMenu ) {      
          select = 4;
        }
        if (!clickB){
          switch(select) {
            case 1: 
              theme = "animaux";
              break;
            case 2: 
              theme = "chiffres";
              break;
            case 3:              
              theme = "objets";
              break;
            case 4:              
              theme = "lettres";
              break;
          }
          clickBPressed = false;
        }
        else{
          if (select == 2 || select == 3 || select == 4) {
              displayImageMenuTheme("images/menuThemes/theme-animaux.png",xMenu+sizeImageMenu,yMenu-sizeImageMenu/2,sizeImageMenu,sizeImageMenu,true);
          } else {
              displayImageMenuTheme("images/menuThemes/theme-animaux.png",xMenu+sizeImageMenu,yMenu-sizeImageMenu/2,sizeImageMenu,sizeImageMenu,false); 
          }
          if (select == 1 || select == 3 || select == 4) {
              displayImageMenuTheme("images/menuThemes/theme-chiffres.png",xMenu-sizeImageMenu*2,yMenu-sizeImageMenu/2,sizeImageMenu,sizeImageMenu,true);
          } else {
              displayImageMenuTheme("images/menuThemes/theme-chiffres.png",xMenu-sizeImageMenu*2,yMenu-sizeImageMenu/2,sizeImageMenu,sizeImageMenu,false);
          }
          if (select == 1 || select == 2 || select == 4) {
              displayImageMenuTheme("images/menuThemes/theme-objets.png",xMenu-sizeImageMenu/2,yMenu-sizeImageMenu*2,sizeImageMenu,sizeImageMenu,true);
          } else {
              displayImageMenuTheme("images/menuThemes/theme-objets.png",xMenu-sizeImageMenu/2,yMenu-sizeImageMenu*2,sizeImageMenu,sizeImageMenu,false);
          }
          if (select == 1 || select == 2 || select == 3) {
              displayImageMenuTheme("images/menuThemes/theme-lettres.png",xMenu-sizeImageMenu/2,yMenu+sizeImageMenu,sizeImageMenu,sizeImageMenu,true);
          } else {
              displayImageMenuTheme("images/menuThemes/theme-lettres.png",xMenu-sizeImageMenu/2,yMenu+sizeImageMenu,sizeImageMenu,sizeImageMenu,false);
          }
          
          fill(0, 0, 0);
          textSize(18);
          text("Choix du thème", xMenu-60,yMenu);
          textSize(11);
        }
      }
  }
  
  // Méthode pour afficher les images du menu des thèmes
  public void displayImageMenuTheme(String imageName, float xCoord, float yCoord,float xSize, float ySize, boolean transparency){
    if (transparency) {
         tint(255,126);
    }
    image(loadImage(imageName), xCoord, yCoord, xSize, ySize);
    tint(255,255);
  }
  
  // Possibilité d'utiliser la souris uniquement sur la page d'accueil
  void onMousePress() {
    // Si on clique sur le cube Start
    if(mouseX > xminStart && mouseX < xmaxStart && mouseY > yminStart && mouseY < ymaxStart) {      
      partieStart = true;
    }
    // Si on clique sur le cube Exit
    if(mouseX > xminExit && mouseX < xmaxExit && mouseY > yminExit && mouseY < ymaxExit) {      
      endGame = true;
    }
    // Si on clique sur le cube Score
    if(mouseX > xminScore && mouseX < xmaxScore && mouseY > yminScore && mouseY < ymaxScore) {      
      displayScore = true;
    }
  }

  void drawCube(float x, float y, float z, float size, float angle, String text) { 
    pushMatrix();
  
    // Center in display window
    translate(x, y, z);
   
     noFill();
   
    if (mousePressed) {
      angle1+=.01;
    }
    else {
      if (angle1>0)
        angle1-=.01;
    }
   
    // Rotate cube
    rotateY(angle);
    stroke(255);
    
    // blanc semi-transparent
    fill(253 ,238 ,0); 
    // Transparent cube, just using box() method 
    box(size);
    fill(3, 34, 76);
    textSize(60);
    
    text(text,-70,20,100);  
    popMatrix();
  }
 
  public boolean getEndGame() {
    return endGame;
  } 
  
  public boolean getDisplayScore() {
    return displayScore;
  }
  
  public void setDisplayScore(boolean display) {
    this.displayScore = display;
  }
  public boolean getPartieStart() {
    return partieStart;
  }
}