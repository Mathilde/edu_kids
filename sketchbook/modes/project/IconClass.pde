// Class représentant l'image d'animation pour la bonne ou la mauvaise réponse


class Icon {
  
  float x;
  float y;
  float diametre;
  PImage image;
  
  Icon(float tmpx,float tmpy, float tmpd, String type) {    
    x= tmpx ;
    y = tmpy ;
    diametre = tmpd;
    if(TEXT_GOOD.equalsIgnoreCase(type)) {      
      image = loadImage("images/good.png");
      play = true;
    } else if(TEXT_BAD.equalsIgnoreCase(type)) {         
      image = loadImage("images/bad.png");
    }
    image.resize(250, 250);   
  }

  void ascend(){
    y = y - 3;
    x = x + random(-2,2);
  }

  void display() {    
    image(image, x, y, diametre, diametre);
    delay(500);
  }
  
  void top() {
    if ( y < diametre/2) {      
      y =  diametre/2.0;
    }
  }
  
  void draw() {    
    ascend();
    display();
    top();
  }
}