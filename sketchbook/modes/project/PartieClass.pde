// Classe représentant une partie, c'est à dire n questions
class Partie {
  Table table;
  int score = 0;
  int nbQuestion = 4;
  ArrayList<Question> questions = null;  
  int current = 0;  
  boolean endPartie;

  
  Partie(String _theme) {
    endPartie = false;
    table = loadTable("questions/"+_theme+".csv", "header");
    score = 0;   
    questions = new ArrayList<Question>();
    for(int i = 0 ; i < this.nbQuestion ; i++) {   
      questions.add(new Question(table,_theme));
    }
  }
  
  void draw() {   
    if (current >= nbQuestion) {
      endPartie = true;
    } else {
      questions.get(current).draw();
      if (questions.get(current).getNextQuestion()){
        if (questions.get(current).getQuestionOk()){
          score ++;
        } 
        current ++;
        //delay(200);
      }  
    }
  } 
  
  boolean getEndPartie(){
     return endPartie; 
  }
  
  int getScore(){
     return score; 
  }
}