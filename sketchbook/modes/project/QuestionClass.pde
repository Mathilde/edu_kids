// Classe représentant une question 
class Question { 

  PImage image1, image2, image3, image4;
  String question;
  boolean first = true;
  String goodAnswer;
  String rightAns = "";
  Table table;
  boolean questionOk;
  boolean nextQuestion;
  String pathImage = "";
  String theme;
  
  
  PImage background;
  
  // Positionnement de l'image 1
  float xminImage1 = (width/2)-280;
  float yminImage1 = height/3-20;
  float xmaxImage1 = xminImage1 + 240;
  float ymaxImage1 = yminImage1 + 240;
  
  // Positionnement de l'image 2
  float xminImage2 = (width/2)+80;
  float yminImage2 = height/3-20;
  float xmaxImage2 = xminImage2 + 240;
  float ymaxImage2 = yminImage2 + 240;
  
  // Positionnement de l'image 3  
  float xminImage3 = (width/2)-280;
  float yminImage3 = height/1.5-20;
  float xmaxImage3 = xminImage3 + 240;
  float ymaxImage3 = yminImage3 + 160;
  
  // Positionnement de l'image 4  
  float xminImage4 = (width/2)+80;;
  float yminImage4 = height/1.5-20;
  float xmaxImage4 = xminImage4 + 240;
  float ymaxImage4 = yminImage4 + 160;
  
  
  Question(Table _table, String _theme) {
    questionOk = false;
    nextQuestion = false;
    table = _table;
    theme = _theme;
    pathImage="images/"+theme+"/";
    background = loadImage(pathImage+"background.png");
    background.resize(width, height);
  }
  
  // Méthode qui permet de sélectionner la question qui va s'afficher
  void loadQuestion() {
    // choix aléatoire des questions
    int index = int(random(table.getRowCount()));  
    TableRow row = table.getRow(index);  
    
    question = row.getString("question");
    goodAnswer =  row.getString("reponse1");
    shakeAnswer(row);
    table.removeRow(index);
  }
  
  // Méthode qui mélange les réponses pour que la bonne réponse ne soit pas toujours la première
  void shakeAnswer(TableRow row) {          
    StringList answers = new StringList();
    for(int i = 1 ; i <= 4 ; i++) {
      answers.append(row.getString("reponse"+i));
    }
    answers.shuffle();
    
    image1 = loadImage(pathImage+answers.get(0));
    image2 = loadImage(pathImage+answers.get(1));
    image3 = loadImage(pathImage+answers.get(2));
    image4 = loadImage(pathImage+answers.get(3));
    
    for(int i = 0 ; i < 4 ; i++) {
      if(answers.get(i).equals(goodAnswer)) {
        rightAns = "Images " + (i+1);
      }
    }
  }
  
  void draw() {          
    if(first) {
      loadQuestion();
      first = false;
    }
    
    // Affichage du background, en fonction du thème
    background(background);
    
    // Affichage du footer
    displayFooter();     
    
    
    // affichage de la question
    textSize(35);
    fill(50);
    textAlign(CENTER);
    text(question, width/2, height/4);  
    
    // Affichage de l'image 1 
    noFill();
    stroke(50);
    rect(xminImage1-20,yminImage1-20,240,240);
    image(image1,xminImage1,yminImage1,200,200);
    textSize(30);
    fill(50);
    text("Image 1", xminImage1+100, yminImage1+260);
  
    // Affichage de l'image 2 
    noFill();
    stroke(50);
    rect(xminImage2-20,yminImage2-20,240,240);
    image(image2,xminImage2,yminImage2,200,200);
    textSize(30);
    fill(50);
    text("Image 2", xminImage2+100, yminImage2+260);
    
    // Affichage de l'image 3 
    noFill();
    stroke(50);
    rect(xminImage3-20,yminImage3-20,240,240);
    image(image3,xminImage3,yminImage3,200,200);
    textSize(30);
    fill(50);
    text("Image 3", xminImage3+100, yminImage3+260);
  
    // Affichage de l'image 4
    noFill();
    stroke(50);
    rect(xminImage4-20,yminImage4-20,240,240);
    image(image4,xminImage4,yminImage4,200,200);
    textSize(30);
    fill(50);
    text("Image 4", xminImage4+100, yminImage4+260);
    
    // Récupération des actions de la manette
    if(stick != null) {
      getUserInput();
        displayJoystick();
    }
    
    // On vérifie la réponse donnée à voix haute
    checkAnswer();
    voiceM = "";
    delay(50);
  }
  
  // Méthode pour récupérer l'action avec la manette de l'utilisateur 
  public void getUserInput() {
      boolean clickA;
      
      px = map(stick.getSlider(1).getValue(), -1, 1, 0, width);
      py = map(stick.getSlider(0).getValue(), -1, 1, 0, height);
      clickA = stick.getButton(0).pressed();
      
      // Si on clique sur l'image 1
      if(clickA && px > xminImage1 && px < xmaxImage1 && py > yminImage1 && py < ymaxImage1) {  
        if("Images 1".equalsIgnoreCase(rightAns)) {                
          println("GOOD Answer");
          displayAnimation(TEXT_GOOD);
          questionOk = true;
          nextQuestion = true;            
        } else {          
          println("BAD Answer");
          displayAnimation(TEXT_BAD);
          nextQuestion = true;
        }
      }
      
      // Si on clique sur l'image 2
      if(clickA && px > xminImage2 && px < xmaxImage2 && py > yminImage2 && py < ymaxImage2) {  
        if("Images 2".equalsIgnoreCase(rightAns)) {                
          println("GOOD Answer");
          displayAnimation(TEXT_GOOD);
          questionOk = true;
          nextQuestion = true;    
        } else {          
          println("BAD Answer");
          displayAnimation(TEXT_BAD);
          nextQuestion = true;
        }
      }
      
      // Si on clique sur l'image 3
      if(clickA && px > xminImage3 && px < xmaxImage3 && py > yminImage3 && py < ymaxImage3) {  
        if("Images 3".equalsIgnoreCase(rightAns)) {                
          println("GOOD Answer");
          displayAnimation(TEXT_GOOD);
          questionOk = true;
          nextQuestion = true;    
        } else {          
          println("BAD Answer");
          displayAnimation(TEXT_BAD);
          nextQuestion = true;
        }
      }
      
      // Si on clique sur l'image 4
       if(clickA && px > xminImage4 && px < xmaxImage4 && py > yminImage4 && py < ymaxImage4) {  
        if("Images 4".equalsIgnoreCase(rightAns)) {                
          println("GOOD Answer");
          displayAnimation(TEXT_GOOD);
          questionOk = true;
          nextQuestion = true;    
        } else {          
          println("BAD Answer");
          displayAnimation(TEXT_BAD);
          nextQuestion = true;
        }
      }
  }
  
  // Méthoode qui vérifie la bonne réponse avec la reconnaissance vocale
  void checkAnswer() {
    if(voiceM.equalsIgnoreCase("Images 1") || voiceM.equalsIgnoreCase("Images 2") 
      || voiceM.equalsIgnoreCase("Images 3") || voiceM.equalsIgnoreCase("Images 4")
      || voiceM.equalsIgnoreCase("images 1") || voiceM.equalsIgnoreCase("images 2") 
      || voiceM.equalsIgnoreCase("images 3") || voiceM.equalsIgnoreCase("images 4")
      || voiceM.equalsIgnoreCase("images de")) {
        if(voiceM.equalsIgnoreCase(rightAns)) {
        println("GOOD Answer");
        displayAnimation(TEXT_GOOD);
        questionOk = true;
        nextQuestion = true;
      } else {
        println("BAD Answer");
        displayAnimation(TEXT_BAD);
        nextQuestion = true;
      }
    }
  }  
  
  void displayAnimation(String type) {
    Icon[]icons = new Icon[4];
    for(int i =0; i< icons.length ; i++){
      icons[i] = new Icon(width/3+i*150,height/2,random(60,150), type);
    }
    if(play) {
      goodSound.play();
    }
    for(int i =0; i< icons.length ; i++){
      icons[i].draw() ; 
    }
    
    if(play) {
      delay(2000);
      goodSound.stop();
      play = false;
    }
  }
 
  boolean getQuestionOk(){
        return questionOk;
  }
  
  boolean getNextQuestion(){
        return nextQuestion;
  }   
}