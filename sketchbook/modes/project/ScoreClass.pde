// Classe représentant la page de score

class Score {
  
  // Positionnement image retour
  int xminBack = width-160;
  int yminBack = height-160;
  int xmaxBack = xminBack + 120;
  int ymaxBack = yminBack + 120;
  
  boolean scoreDisplay = false;
  int scoreLastGame = -1;
  PImage background;
  
  public Score() {} 
  
  void draw() {
    // Affichage du background
    background = loadImage("images/background_score.jpg");
    background.resize(width, height);
    background(background);
    
    // Affichage du footer
    displayFooter();    
    
    // Affichage du résultat
    textSize(35);
    textAlign(CENTER);
    fill(255, 0, 0);
    if(scoreLastGame == -1) {  
      fill(255, 0, 0);    
      text("Vous n'avez pas encore fait de partie...", width/2, height/3);
    } else {      
      fill(0, 0, 255);
      text("Score de la partie précédente : "+scoreLastGame +" !", width/2, height/3);
    }
    
    // Affichage de l'image retour
    image(loadImage("images/back.png"),xminBack,yminBack,120,120);
    
    // Récupération des actions de la manette
    if(stick != null) {
      getUserInput();
      displayJoystick();
    }
    delay(50);
  }
  
  // Méthode pour récupérer l'action avec la manette de l'utilisateur 
  public void getUserInput() {
      boolean clickA;
      
      px = map(stick.getSlider(1).getValue(), -1, 1, 0, width);
      py = map(stick.getSlider(0).getValue(), -1, 1, 0, height);
      clickA = stick.getButton(0).pressed();
      
      // Si on clique sur l'image retour
      if(clickA && px > xminBack && px < xmaxBack && py > yminBack && py < ymaxBack) {      
         scoreDisplay = false;
      }
  }
  
  void setDisplay(boolean value){
       scoreDisplay = value;
  }
      
  boolean getDisplay(){
        return scoreDisplay;
  }
  
  void setScoreLastGame(int value){
    scoreLastGame = value;
  }
}