///** RV ***/
///*
//* Open this page : https://codepen.io/getflourish/pen/NpBGqe
//*  after running the app ! 
//* recognition.lang = "fr-FR";
//*
//*/
//* Programme principal */

import websockets.*;
import org.gamecontrolplus.gui.*;
import org.gamecontrolplus.*;
import net.java.games.input.*;
import processing.sound.*;

// Variables globales de texte
private static String TEXT_GAME_NAME = "Edu'Kids";
private static String TEXT_FOOTER = "Imen, Mathilde & Mohamed - "+TEXT_GAME_NAME;
private static String TEXT_GOOD = "good";
private static String TEXT_BAD = "bad";

// Variables pour la reconnaissance vocale
WebsocketServer socket;
static String voiceM="start";

// Varibles pour la manette
ControlIO control;
ControlDevice stick = null;

// Variables pour le son de la bonne réponse
static SoundFile goodSound;
static boolean play = false;

Accueil accueil;
boolean clickBPressed = false;
float xMenu,yMenu;
float px, py;

public void settings() {  
  size(1700,970, P3D);
}

void setup() {
  socket = new WebsocketServer(this, 1337, "/p5websocket");    
  accueil = new Accueil();
  goodSound = new SoundFile(this, "sounds/sample.mp3");
  
  // On récupère la manette, si elle fait partie des devices connectés
  control = ControlIO.getInstance(this);
  for (ControlDevice device : control.getDevices()) {
    if("Controller (XBOX 360 For Windows)".equalsIgnoreCase(device.getName())) {    
      stick = control.getDevice("Controller (XBOX 360 For Windows)");
      break;
    }
  }
  // Si la manette n'est pas reconnu, on met un message d'erreur et on continue
  if(stick == null) {
      println("No suitable device configured");
    } 
  }

void draw() {   
  accueil.draw();
}

void mousePressed() {
  accueil.onMousePress();
}

void displayJoystick() {
  fill(0, 102, 0, 32);
  noStroke();
  fill(0, 102, 0, 128);
  ellipse(px, py, 30, 30);
}

void displayFooter() {  
  textSize(20);
  textAlign(CENTER);
  fill(3, 34, 76);
  text(TEXT_FOOTER, width/2, height-20);
}
  
void webSocketServerEvent(String msg) {    
  print("intial message:"+msg+"\n");
  if(msg.substring(0,1).equals(" ")){
    voiceM = msg.substring(1);}
  else{
    voiceM=msg;
  }
  print("final message :"+voiceM+"\n");
}